<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: block to show subscriptions in customer dashboard
 */

namespace Shero\Notifications\Block\Account;

use Magento\Framework\View\Element\Template;
use Shero\Notifications\Model\Notifications;
use \Magento\Customer\Model\Session;
use Shero\Notifications\Model\ResourceModel\Notifications\CollectionFactory;


class Subscriptions extends \Magento\Framework\View\Element\Template
{

    protected $_customerSession;
    protected $_collectionFactory;

    public function __construct(
        Template\Context $context,
        CollectionFactory $_collectionFactory,
        Session $_customerSession,
        array $data = []
    )
    {
        $this->_collectionFactory = $_collectionFactory;
        $this->_customerSession = $_customerSession;
        parent::__construct($context, $data);
    }

    public function getSubscriptions()
    {
        // get customer email
        $customerEmail = $this->_customerSession->getCustomer()->getEmail();
        $subscriptions = $this->_collectionFactory->create();

        //show customer subscriptions
        $subscriptions->addFieldToFilter('customer_email', $customerEmail);

        return $subscriptions;


    }
}