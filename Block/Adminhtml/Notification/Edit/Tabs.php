<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: tab container in admin
 */
namespace Shero\Notifications\Block\Adminhtml\Notification\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('notification_tab');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Notification Information'));
    }

}
