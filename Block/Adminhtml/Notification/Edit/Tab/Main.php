<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: prepare admin form fields
 */
namespace Shero\Notifications\Block\Adminhtml\Notification\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    protected $store;
    protected $contex;
    protected $storeSource;


    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Source\Store $storeSource,
        array $data = []
    ) {
        $this->context = $context;
        parent::__construct($context,$registry,$formFactory,$data);
    }


    protected function _prepareForm()
    {


        $groups = $this->_coreRegistry->registry('subscription_data');
        $form = $this->_formFactory->create();


        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Notification Information')]);

        if ($groups != "") {
            $fieldset->addField('alert_id', 'hidden', ['name' => 'alert_id']);
        }



        $fieldset->addField(
            'id',
            'text',
            [
                'name' => 'id',
                'label' => __('Product ID'),
                'title' => __('Product ID'),
                'required' => true,
                'class' => 'validate-number'
            ]
        );


        $fieldset->addField(
            'customer_email',
            'text',
            [
                'name' => 'customer_email',
                'label' => __('Customer Email'),
                'title' => __('Customer Email'),
                'required' => true,
                'class' => 'validate-email'
            ]
        );



        if ($groups != "") {
            $form->setValues($groups->getData());
        }


        $this->setForm($form);

        return parent::_prepareForm();

    }


    public function getTabLabel()
    {
        return __('Notification Information');
    }


    public function getTabTitle()
    {
        return __('Notification Information');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }


    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }


}
