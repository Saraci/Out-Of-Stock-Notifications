<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: block for form in admin
 */
namespace Shero\Notifications\Block\Adminhtml\Notification;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'alert_id';
        $this->_blockGroup = 'Shero_Notifications';
        $this->_controller = 'adminhtml_notification';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Subscriber'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                    ],
                ]
            ],
            -100
        );

        $this->buttonList->update('delete', 'label', __('Delete Notification'));
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('notifications/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }

    public function getValidationUrl()
    {
        return $this->getUrl('notifications/*/validate', ['_current' => true]);
    }
    public function getBackUrl()
    {
        return $this->getUrl('*/grid');
    }

}
