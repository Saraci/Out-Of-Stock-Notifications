<?php
/**
 * Copyright (c) 2017 Sherodesigns.
 * *
 * @Project besienterprise.dev
 * @date 6/7/2017
 * @author Sherodesigns | Besim Saraci
 * @link https://www.sherodesigns.com/
 * @module Notifications_Block
 * @class Notifications_Block_Plugin
 * @description: prevent out of stock options being disabled
 */

namespace Shero\Notifications\Block;

class Plugin
{
    /**
     * getAllowProducts prevent out of stock options being disabled
     *
     * @param \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject
     *
     * @return array
     */
    public function beforeGetAllowProducts($subject)
    {
        if (!$subject->hasData('allow_products')) {
            $products = [];
            $allProducts = $subject->getProduct()->getTypeInstance()->getUsedProducts($subject->getProduct(), null);
            foreach ($allProducts as $product) {
                $products[] = $product;
            }
            $subject->setData('allow_products', $products);
        }

        return [];
    }

}