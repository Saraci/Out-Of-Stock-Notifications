<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: block to get configurable product options
 */

namespace Shero\Notifications\Block\Product\View\Type;

use Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Configurable extends \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable
{

    /**
     * Catalog product
     *
     * @var \Magento\Catalog\Helper\Product
     */
    protected $catalogProduct = null;

    /**
     * Current customer
     *
     * @var CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * Prices
     *
     * @var array
     */
    protected $_prices = [];

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var \Magento\ConfigurableProduct\Helper\Data $imageHelper
     */
    protected $helper;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var ConfigurableAttributeData
     */
    protected $configurableAttributeData;


    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    protected $_helper;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\ConfigurableProduct\Helper\Data $helper
     * @param \Magento\Catalog\Helper\Product $catalogProduct
     * @param CurrentCustomer $currentCustomer
     * @param PriceCurrencyInterface $priceCurrency
     * @param ConfigurableAttributeData $configurableAttributeData
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\ConfigurableProduct\Helper\Data $helper,
        \Magento\Catalog\Helper\Product $catalogProduct,
        CurrentCustomer $currentCustomer,
        PriceCurrencyInterface $priceCurrency,
        ConfigurableAttributeData $configurableAttributeData,
        array $data = [],
        \Magento\Catalog\Model\Product $product,
        \Shero\Notifications\Helper\Data $_helper
    )
    {
        $this->priceCurrency = $priceCurrency;
        $this->helper = $helper;
        $this->jsonEncoder = $jsonEncoder;
        $this->catalogProduct = $catalogProduct;
        $this->product = $product;
        $this->_helper = $_helper;
        $this->currentCustomer = $currentCustomer;
        $this->configurableAttributeData = $configurableAttributeData;
        parent::__construct(
            $context,
            $arrayUtils,
            $jsonEncoder,
            $helper,
            $catalogProduct,
            $currentCustomer,
            $priceCurrency,
            $configurableAttributeData,
            $data
        );
    }

    /**
     * @return string
     * get all product options ids
     */
    public function getProductInfo()
    {
        /**
         * show configurable options data
         * @var $configurableProduct \Magento\Catalog\Model\Product
         */
        $configurableProduct = $this->getProduct();

        $typeInstance = $configurableProduct->getTypeInstance();
        $childrensArray = $typeInstance->getChildrenIds($configurableProduct->getId());

        $returnArray = array();

        foreach ($childrensArray as $children) {

            foreach ($children as $productId) {
                if ($this->currentCustomer->getCustomerId()) {
                    $email = $this->currentCustomer->getCustomer()->getEmail();
                    $subscriptionExists = $this->_helper->subscriptionExists($productId, $email);
                } else {
                    $subscriptionExists = false;
                }
                $product = $this->product->loadByAttribute('entity_id', $productId);
                $isInStock = $this->_helper->getStockInformation($product->getId())->getIsInStock();

                /**
                 * product out of stock && subscription not exists
                 */
                if (!$subscriptionExists && !$isInStock) {
                    $isInStock = 1;

                }
                /**
                 * product out of stock && subscription  exists
                 */
                elseif ($subscriptionExists and !$isInStock) {
                    $isInStock = 2;
                }
                /**
                 * product in stock
                 */
                else {
                    $isInStock = 0;
                }

                $returnArray[$productId] = array('stock' => $isInStock);
            }
        }
        return json_encode($returnArray);

    }

}
