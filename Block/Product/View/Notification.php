<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: block to show subscription form in product page
 */

namespace Shero\Notifications\Block\Product\View;

use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Framework\Registry;
use Magento\Catalog\Block\Product\Context;

class Notification extends AbstractProduct
{

    protected $_currentProduct;
    public function __construct(
        Context $context,
        Registry $_currentProduct ,
        array $data = [])
    {
        $this->_currentProduct = $_currentProduct;
        parent::__construct($context, $data);
    }

    public function getFormActionUrl()
    {
        return $this->getUrl('notifications/notification/new', ['_secure' => true]);
    }
     public function getCurrentProduct (){

        return $this->_currentProduct->registry('current_product');
     }

}