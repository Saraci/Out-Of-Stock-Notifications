# README #

This extension is used for managing out of stock alerts in Magento 2. It provides extra functionalities from what default magento installation gives.
* Manage subscriptions in admin grid.
* Show subscriptions in customer dashboard
* Manage stock alerts for configurable products

# Out of Stock Notification #

## *Version 1.0.0 - June 5, 2017* ##

### Content: ###


* Module: Out of Stock Notification
* Version: 1.0.0
* Copyright: SheroDesigns

### How to set up: ###

* Clone the extension to app/code directory
* run php bin/magento setup:upgrade
clean cache: php bin/magento cache:clean

### Compatibility ###

* Magento CE 2.0.0 - 2.1.7
* Magento EE 2.0.0 - 2.1.7

### Developers: ###

* Ylgen Guxholli
* Besim Saraci

* Copyright &copy; 2017 Sherodesigns | All rights reserved.


1. Extension Information
1. General Settings
1. Subscribe for Simple Products
1. Subscribe for Configurable Products
1. Manage Subscriptions in Admin
1. Manage Subscriptions in Frontend
1. Low Stock Notifications
1. Email Templates



## Extension Information: ##

This extension is used for managing out of stock alerts in Magento 2. It provides extra functionalities from what default magento installation gives .

* Out of Stock Notification remind the customer when the product he want is back in stock.
* Available for Guest and Logged in Users also for Simple and Configurable Products . 
* Manage Subscriptions in Admin and Frontend
* Send emails manually and automatically with cron.

## General Settings: ##

Find the configuration of Out of Stock extension by going into admin panel:
**Stores -> Configuration -> Notifications**

![1.png](https://bitbucket.org/repo/AggLodB/images/1320194355-1.png)

* **Receiver emails:** admins that will receive email for low stock
* **Sender Email:** sender email address
* **Sender Name:** name of the sender
* **Product Back in Stock email:** email that will be used to notify customers when products come back in stock
* **Admin Notification Email:** email template that will be used to notify admins for low stock products
* **Stock Status Email:** email template for the emails that will be sent from notifications grid in admin

## Subscribe for Simple Products: ##

![1.png](https://bitbucket.org/repo/AggLodB/images/3174938257-1.png)

## Subscribe for Configurable Products: ##

![1.png](https://bitbucket.org/repo/AggLodB/images/2007514753-1.png)

## When have subscribed: ##

![1.png](https://bitbucket.org/repo/AggLodB/images/1480627038-1.png)

## Manage Subscriptions in Admin: ##

![1.png](https://bitbucket.org/repo/AggLodB/images/3860691550-1.png)

## Manage Subscriptions  in Frontend: ##

![1.png](https://bitbucket.org/repo/AggLodB/images/3206647295-1.png)

## Low Stock Notifications: ##

![1.png](https://bitbucket.org/repo/AggLodB/images/4060120033-1.png)

## Email Templates: ##

There are 3 types of Email Templates that need to be generated manually from the admin.

Go to **Marketing -> Email Templates -> Add New Template**

**Load each of the three templates under Shero_Notifications**

![Out_of_Stock_User_Guide_-_Google_Docs.png](https://bitbucket.org/repo/AggLodB/images/2298659243-Out_of_Stock_User_Guide_-_Google_Docs.png)

### Save the Templates ###

![Out_of_Stock_User_Guide_-_Google_Docs.png](https://bitbucket.org/repo/AggLodB/images/170601801-Out_of_Stock_User_Guide_-_Google_Docs.png)

### Generate the Templates on Shero Notifications Configurations ###
**(Stores > Configuration > Notifications) and Save the Configurations:**

![Out_of_Stock_User_Guide_-_Google_Docs.png](https://bitbucket.org/repo/AggLodB/images/909553179-Out_of_Stock_User_Guide_-_Google_Docs.png)

![Out_of_Stock_User_Guide_-_Google_Docs.png](https://bitbucket.org/repo/AggLodB/images/1801343651-Out_of_Stock_User_Guide_-_Google_Docs.png)

![Out_of_Stock_User_Guide_-_Google_Docs.png](https://bitbucket.org/repo/AggLodB/images/3364823366-Out_of_Stock_User_Guide_-_Google_Docs.png)

