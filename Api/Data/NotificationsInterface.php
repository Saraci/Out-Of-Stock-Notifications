<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Notifications\Api\Data;

interface NotificationsInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    const ALERT_ID      = 'alert_id';
    const ALERT_CUSTOMER_NAME    = 'customer_name';
    const ALERT_PROD_ID   = 'id';
    const ALERT_PRODUCT_NAME = 'product_name';
    const ALERT_CUSTOMER_EMAIL = 'customer_email';
    const ALERT_SUBSCRIPTION_DATE = 'subscription_date';
    const ALERT_STATUS = 'status';


    public function getAlertId();

    public function setAlertId($entityId);

    public function getAlertCustomerName();

    public function setAlertCustomerName($customer_name);

    public function getAlertProductId();

    public function setAlertProductId($id);

    public function getAlertProductName();

    public function setAlertProductName($product_name);

    public function getAlertCustomerEmail();

    public function setAlertCustomerEmail($customer_email);

    public function getAlertSubscriptionDate();

    public function setAlertSubscriptionDate($subscription_date);

    public function getAlertStatus();

    public function setAlertStatus($status);

}