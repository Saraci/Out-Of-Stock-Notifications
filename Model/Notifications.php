<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Notifications\Model;

use Shero\Notifications\Api\Data\NotificationsInterface;

class Notifications extends \Magento\Framework\Model\AbstractExtensibleModel implements NotificationsInterface
{
    protected function _construct()
    {
        $this->_init('Shero\Notifications\Model\ResourceModel\Notifications');
    }

    public function getAlertId(){
        return $this->getData(self::ALERT_ID);
    }

    public function setAlertId($entityId){
        $this->setData(self::ALERT_ID, $entityId);
    }

    public function getAlertCustomerName(){
        return $this->getData(self::ALERT_CUSTOMER_NAME);
    }

    public function setAlertCustomerName($customer_name){
        $this->setData(self::ALERT_CUSTOMER_NAME, $customer_name);
    }

    public function getAlertProductId(){
        return $this->getData(self::ALERT_PROD_ID);
    }

    public function setAlertProductId($id){
        $this->setData(self::ALERT_PROD_ID, $id);
    }

    public function getAlertProductName(){
        return $this->getData(self::ALERT_PRODUCT_NAME);
    }

    public function setAlertProductName($product_name){
        $this->setData(self::ALERT_PRODUCT_NAME, $product_name);
    }


    public function getAlertCustomerEmail(){
        return $this->getData(self::ALERT_CUSTOMER_EMAIL);
    }

    public function setAlertCustomerEmail($customer_email){
        $this->setData(self::ALERT_CUSTOMER_EMAIL, $customer_email);
    }


    public function getAlertSubscriptionDate(){
        return $this->getData(self::ALERT_SUBSCRIPTION_DATE);
    }


    public function setAlertSubscriptionDate($subscription_date){
        $this->setData(self::ALERT_SUBSCRIPTION_DATE, $subscription_date);
    }

    public function getAlertStatus(){
        return $this->getData(self::ALERT_STATUS);
    }


    public function setAlertStatus($status){
        $this->setData(self::ALERT_STATUS, $status);
    }


    public function getCustomAttributesCodes() {
        return array('alert_id', 'customer_name' ,'id','product_name','customer_email','subscription_date' ,'status');
    }
}

