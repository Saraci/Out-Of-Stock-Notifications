<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: notifications collection model
 */
namespace Shero\Notifications\Model\ResourceModel\Notifications;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection implements \Magento\Framework\Api\Search\SearchResultInterface
{
    /**
     * @var string
     */
    protected $_idFieldName = 'alert_id';

    protected $aggregations;

    protected function _construct()
    {
        $this->_init('Shero\Notifications\Model\Notifications', 'Shero\Notifications\Model\ResourceModel\Notifications');
    }


    /**
     * @param array|null $items
     * @return $this
     */
    public function setItems(array $items = null){
        return $this;
    }


    /**
     * @return mixed
     */
    public function getAggregations(){
        return  $this->aggregations;
    }

    /**
     * @param \Magento\Framework\Api\Search\AggregationInterface $aggregations
     */
    public function setAggregations($aggregations){
        $this->aggregations = $aggregations;
    }

    /**
     * @return null
     */
    public function getSearchCriteria(){
        return null;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface|null $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null){
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalCount(){
        return $this->getSize();
    }

    /**
     * @param int $totalCount
     * @return $this
     */
    public function setTotalCount($totalCount){
        return $this;
    }
}