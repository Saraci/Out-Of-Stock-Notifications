<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Notifications\Model\ResourceModel;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Notifications extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected  $scopeConfig;


    public function __construct(

        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {

        parent::__construct($context);
    }

    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
            $this->_init('shero_stock_notifications', 'alert_id');

    }

}