<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description
 */
namespace Shero\Notifications\Ui\Component\Form;

use Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool;
use Shero\Notifications\Model\ResourceModel\Notifications\Collection;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Shero\Notifications\Model\ResourceModel\Notifications\Collection
     */
    protected $collection;

    /**
     * @var \Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool
     */
    protected $filterPool;

    /**
     * @var array
     */
    protected $loadedData;


    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Shero\Notifications\Model\ResourceModel\Notifications\CollectionFactory $collectionFactory
     * @param \Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool $filterPool
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        FilterPool $filterPool,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->filterPool = $filterPool;
    }


    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->loadedData) {
            $items = $this->collection->getItems();
            $result = array();
            foreach ($items as $item) {
                $result['shero_notifications'] = $item->getData();
                $this->loadedData[$item->getAlertId()] = $result;
                break;
            }
        }
        return $this->loadedData;
    }
}
