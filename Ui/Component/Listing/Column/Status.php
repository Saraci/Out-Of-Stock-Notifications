<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: set value to Active = 1, Inactive = 0
 */
namespace Shero\Notifications\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Status extends \Magento\Ui\Component\Listing\Columns\Column
{

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $data = [],
        array $components = []
    ) {

        parent::__construct($context, $uiComponentFactory, $components, $data);
    }


    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {


            $fieldName = $this->getData('name');

            foreach ($dataSource['data']['items'] as & $item) {

                if (isset($item[$fieldName])) {
                    if($item[$fieldName] == '0'){
                        $item[$fieldName] = "Inactive";
                    }
                    else {
                    $item[$fieldName] = "Active";
                }
                }
            }
        }

        return $dataSource;
    }
}