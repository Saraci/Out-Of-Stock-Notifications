<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */

namespace Shero\Notifications\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{


    const NOTIFICATION_RECEIVER_EMAILS = 'notifications/general/receiver_emails';
    const NOTIFICATION_SENDER_EMAIL = 'notifications/general/sender_email';
    const NOTIFICATION_SENDER_NAME = 'notifications/general/sender_name';
    const NOTIFICATION_ADMIN_EMAIL = 'notifications/general/admin_email';
    const NOTIFICATION_STOCK_EMAIL = 'notifications/general/status_email';
    const NOTIFICATION_MAIN_EMAIL = 'notifications/general/stock_email';
    protected $_customerSession;
    protected $_productRepository;
    protected $_customerRepository;
    protected $_stockItemRepository;
    protected $_accountManager;
    protected $_backendUrl;
    protected $_notificationsFactory;
    protected $_scopeConfig;
    protected $_storeManager;
    protected $_collectionFactory;
    protected $_productCollectionFactory;
    protected $_configurable;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $_customerSession,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Customer\Api\AccountManagementInterface $_accountManager,
        \Magento\Catalog\Model\ProductRepository $_productRepository,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $_stockItemRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $_customerRepository,
        \Shero\Notifications\Model\NotificationsFactory $_notificationsFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $_scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $_storeManager,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $_configurable,
        \Shero\Notifications\Model\ResourceModel\Notifications\CollectionFactory $_collectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $_productCollectionFactory


    )
    {
        $this->_customerSession = $_customerSession;
        $this->_productRepository = $_productRepository;
        $this->_accountManager = $_accountManager;
        $this->_backendUrl = $backendUrl;
        $this->_stockItemRepository = $_stockItemRepository;
        $this->_customerRepository = $_customerRepository;
        $this->_notificationsFactory = $_notificationsFactory;
        $this->_scopeConfig = $_scopeConfig;
        $this->_storeManager = $_storeManager;
        $this->_configurable =$_configurable;
        $this->_collectionFactory = $_collectionFactory;
        $this->_productCollectionFactory = $_productCollectionFactory;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->_customerSession->isLoggedIn();
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {

        if ($this->isLoggedIn()) {
            return $this->_customerSession->getCustomer()->getEmail();
        }
        return false;
    }

    /**
     * return product url, if option return parent url
     * @param $product
     * @return mixed
     */
    public function getProductUrl($product){

        if($product->getVisibility() == 1)
        {

            $product = $this->_configurable->getParentIdsByChild($product->getId());
            if(isset($product[0])){

                 return $this->getProductById($product[0])->getProductUrl();
            }
        }
        else{
            return $product->getProductUrl();
        }
    }
    /**
     * @param $id
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed
     */
    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_backendUrl->getUrl($route, $params);
    }

    /**
     * @param $email
     * @return bool
     */
    public function customerExist($email)
    {
        if ($this->_accountManager->isEmailAvailable($email)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $email
     * @return string
     */
    public function getCustomerName($email)
    {
        $customer = $this->_customerRepository->get($email);
        return $customer->getFirstname();
    }

    /**
     * @return array|mixed
     */
    public function getAdminReceiverEmails()
    {

        $adminEmails = $this->_scopeConfig->getValue(self::NOTIFICATION_RECEIVER_EMAILS);
        $adminEmails = explode(',', $adminEmails);
        return $adminEmails;
    }

    /**
     * @return mixed
     */
    public function getAdminSenderEmail()
    {

        return $adminEmails = $this->_scopeConfig->getValue(self::NOTIFICATION_SENDER_EMAIL);
    }

    /**
     * @return mixed
     */
    public function getAdminSenderName()
    {

        return $adminEmails = $this->_scopeConfig->getValue(self::NOTIFICATION_SENDER_NAME);
    }


    /**
     * @return mixed
     */
    public function getAdminEmailTemplate()
    {

        return $this->_scopeConfig->getValue(self::NOTIFICATION_ADMIN_EMAIL);
    }

    /**
     * @return mixed
     */
    public function getStockStatusEmailTemplate()
    {

        return $this->_scopeConfig->getValue(self::NOTIFICATION_STOCK_EMAIL);
    }

    /**
     * @return mixed
     */
    public function getEmailNotificationTemplate()
    {

        return $this->_scopeConfig->getValue(self::NOTIFICATION_MAIN_EMAIL);
    }

    /**
     * @param $productId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     */
    public function getStockItem($productId)
    {
        return $this->_stockItemRepository->get($productId);
    }

    /**
     * @param $id
     * @param $email
     * @return bool
     */
    public function subscriptionExists($id, $email)
    {

        $collection = $this->_collectionFactory->create();
        $collection->addFieldToFilter('id', $id);
        $collection->addFieldToFilter('customer_email', $email);
        $collection->addFieldToFilter('status', '1');

        if (count($collection) > 0) {
            return true;
        } else {

            return false;
        }

    }

    /**
     * we expect the item to be the sku of the product or the product model when we have it
     * @param $item string | \Magento\Catalog\Model\Product
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     */
    public function getStockInformation($item)
    {

        if ($item instanceof \Magento\Catalog\Model\Product) {
            return $this->_stockItemRepository->get($item);
        } else {
            return $this->_stockItemRepository->get($this->getProductById($item)->getId());
        }

    }

    /**
     * @return string
     */
    public function getStoreName()
    {

        return $this->_storeManager->getStore()->getName();
    }

    /**
     * @param $id
     * @return bool
     */
    public function isLowStock($id)
    {
        $alertQty = 0;

        if ($this->getStockInformation($id)->getIsInStock()) {
            $currentQty = $this->getStockInformation($id)->getQty();

            if ($this->getProductById($id)->getCustomAttribute('stock_alert') != null) {

                $alertQty = $this->getProductById($id)->getCustomAttribute('stock_alert')->getValue();
            }

            if (($currentQty - $alertQty) > 0) {
                return false;
            }

            return true;
        }
    }

    public function isOutOfStock($id)
    {

        return !($this->getStockInformation($id)->getIsInStock());

    }

    /**
     * check if form should be shown
     * @param $product \Magento\Catalog\Model\Product
     * @return bool|\Magento\CatalogInventory\Api\Data\StockItemInterface
     */
    public function showSubscriptionForm($product)
    {

        if ($this->isConfigurable($product)) {
            return true;
        } else {
            $stock = $this->_stockItemRepository->get($product->getId());
            $customerEmail = $this->getCustomerEmail();
            if ($customerEmail == false) {
                if ($stock->getIsInStock() == 0) {
                    return true;
                }
            } else {
                if ($this->subscriptionExists($product->getId(), $customerEmail) == false) {
                    if ($stock->getIsInStock() == 0) {
                        return true;
                    }
                }
            }

            return false;
        }

    }

    /**
     * show message if customer is subscribed
     * @param  $product \Magento\Catalog\Model\Product
     * @return bool
     */
    public function showIsSubscribedNotification($product)
    {

        if ($this->isConfigurable($product)) {
            return true;
        } else {

            $customerEmail = $this->getCustomerEmail();
            if ($customerEmail and $this->subscriptionExists($product->getId(), $customerEmail) == true
                and $product->getIsInStock() == 0
            ) {

                return true;
            }
        }
        return false;
    }

    /**
     * @param $product
     * @return bool
     * check if product is configurable
     */
    public function isConfigurable($product)
    {
        if ($product->getTypeId() == 'configurable') {
            return true;
        }
        return false;
    }

    /**
     * @return array | get list of all out of stock products
     */
    public function getOutOfStockProducts()
    {

        /**
         * @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection
         */
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');


        $collection->getSelect()->join(
            'cataloginventory_stock_item',
            'e.entity_id = cataloginventory_stock_item.product_id',
            '*');

        $outOfStockProducts = array();

        /**
         * $item \Magento\Catalog\Model\Product
         */
        foreach ($collection as $item) {
            $product = $this->getProductById($item->getEntityId());
            $stockAlert = $product->getCustomAttribute('stock_alert');
            if ($stockAlert) {
                $stockAlert = $stockAlert->getValue();
                if ($stockAlert >= $item->getQty()) {
                    $outOfStockProducts [] = ([

                        'id' => $item->getEntityId(),
                        'name' => $item->getName(),
                        'stock' => $item->getQty()
                    ]);

                }
            }


        }


        return $outOfStockProducts;

    }

}
