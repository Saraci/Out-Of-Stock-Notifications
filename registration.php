<?php
/**
 * Copyright (c) 2017 Sherodesigns.
 * *
 * @date 5/16/2017
 * @author Sherodesigns | Besim Saraci
 * @link https://www.sherodesigns.com/
 * @module Notifications_registration.php
 * @class Notifications_registration
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Shero_Notifications',
    __DIR__
);