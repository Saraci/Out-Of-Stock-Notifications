<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: save controller for subscription form
 */
namespace Shero\Notifications\Controller\Notification;

use Magento\Framework\App\Action\Context;
use Shero\Notifications\Model\NotificationsFactory;
use \Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;

class NewAction extends \Magento\Framework\App\Action\Action
{

    protected $_notificationFactory;
    /**
     * @var \Magento\Catalog\Api\Data\ProductInterface
     */
    protected $_productFactory;

    protected $_customerRepository;

    protected $_session;

    protected $_helper;

    public function __construct(
        Context $context,
        NotificationsFactory $_notificationsFactory,
        ProductFactory $_productFactory ,
        CustomerRepositoryInterface $_customerRepository,
        Session $_session,
        \Shero\Notifications\Helper\Data $_helper,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_notificationFactory = $_notificationsFactory;
        $this->_productFactory = $_productFactory;
        $this->_customerRepository = $_customerRepository;
        $this->messageManager = $messageManager;
        $this->_session = $_session;
        $this->_helper = $_helper;
        parent::__construct($context);
    }

public function execute()
{

    $params = $this->getRequest()->getPost();

    $subscriptionDate = time();
    $email = $params['email'];
    $id = $params['shero-product'];

    if($this->_helper->subscriptionExists($id,$email)){

        $this->messageManager->addWarningMessage(__('You have already subscribed for this product.'));
        $this->_redirect($this->_redirect->getRefererUrl());

    }
    else {

        //check if the customer is logged in
        if (!$this->_session->isLoggedIn()) {

            $customerName = 'Guest';

        } else {
            $customer = $this->_session->getCustomer();
            $email = $customer->getEmail();
            $customerName = $customer->getFirstname();
        }

        try {

            $product = $this->_helper->getProductById($id);
            $productName = $product->getName();
            $model = $this->_notificationFactory->create();
            $model->setAlertCustomerName($customerName);
            $model->setAlertProductName($productName);
            $model->setAlertProductId($id);
            $model->setAlertCustomerEmail($email);
            $model->setAlertStatus('1');
            $model->setAlertSubscriptionDate($subscriptionDate);
            $model->save();
            $this->messageManager->addSuccessMessage(__('You subscribed successfully for this product.'));
            $this->_redirect($this->_redirect->getRefererUrl());

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $this->_redirect($this->_redirect->getRefererUrl());

        }
    }
}
}
