<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: delete controller
 */

namespace Shero\Notifications\Controller\Notification;

use Magento\Framework\App\Action\Context;
use Shero\Notifications\Model\NotificationsFactory;


class Delete extends \Magento\Framework\App\Action\Action
{

    protected $_notificationFactory;


    public function __construct(
        Context $context,
        NotificationsFactory $_notificationsFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_notificationFactory = $_notificationsFactory;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute()
    {

        $alertId = $this->getRequest()->getParam('alert_id');


        try {
            $model = $this->_notificationFactory->create();
            $item = $model->load($alertId);
            $item->delete();
            $this->messageManager->addSuccessMessage(__('Subscription successfully deleted.'));
            $this->_redirect('*/notification');
            return;
        }
        catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Something went wrong while deleting your subscription.'));
            return;
        }

    }
}
