<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: prepare dashboard page for subscriptions
 */

namespace Shero\Notifications\Controller\Notification;
use Magento\Framework\App\Action;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action {

    public function execute() {

        {
            /** @var \Magento\Framework\View\Result\Page resultPage */
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $resultPage->getConfig()->getTitle()->set(__('Out Of Stock Notifications'));
            return $resultPage;
        }
    }

}
?>

