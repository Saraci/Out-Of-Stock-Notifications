<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description
 */

namespace Shero\Notifications\Controller\Adminhtml\Notification\Edit;

use Magento\Backend\App\Action;

class NewAction extends \Magento\Backend\App\Action
{

    protected $_resultForwardFactory;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    )
    {
        $this->_resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Shero_Notifications::manage_notifications');
    }


    public function execute()
    {
        $resultForward = $this->_resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
