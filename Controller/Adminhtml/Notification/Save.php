<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: save controller in admin
 */

namespace Shero\Notifications\Controller\Adminhtml\Notification;

use Braintree\Exception;
use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;
use Magento\Framework\Controller\ResultFactory;
use Shero\Notifications\Model\NotificationsFactory;


class Save extends \Magento\Backend\App\Action
{
    /**
     * @var CacheTypeListInterface
     */
    protected $cache;

    /**
     * @var NotificationsFactory
     */
    protected $_notificationFactory;

    protected $helperData;
    protected $resultFactory;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param CacheTypeListInterface $cache
     * @param NotificationsFactory $notificationsFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        CacheTypeListInterface $cache,
        NotificationsFactory $_notificationsFactory,
        \Shero\Notifications\Helper\Data $helperData

    )
    {

        parent::__construct($context);
        $this->_helper = $helperData;
        $this->cache = $cache;
        $this->_notificationFactory = $_notificationsFactory;
        $this->resultFactory = $context->getResultFactory();

    }

    public function execute()
    {
        $model = $this->_notificationFactory->create();
        $params = $this->getRequest()->getParams();
        $productId = $params['id'];
        $go_back = isset($params['back']) ? true : false;
        $subscriptionDate = time();
        $email = $params['customer_email'];

        if (isset($params['alert_id'])) {

            $alertId = $params['alert_id'];
        } else {
            $alertId = 0;
        }

        //check if requested product exists
        try {

            $product = $this->_helper->getProductById($productId);
            $productId = $product->getId();
        } catch (\Exception $e) {

            $this->messageManager->addError($e->getMessage());
            $this->_redirect($this->_redirect->getRefererUrl());
        }


        //get customer name
        if (!$this->_helper->customerExist($email)) {
            $customerName = 'Guest';
        } else {

            $customerName = $this->_helper->getCustomerName($email);
        }

        //edit
        if ($alertId) {
            if (!$this->_helper->subscriptionExists($productId, $email)) {
                if ($this->_helper->isOutOfStock($productId)) {


                    try {
                        $alertId = $params['alert_id'];
                        $productName = $product->getName();
                        $item = $model->load($alertId);
                        $item->setAlertProductName($productName);
                        $item->setAlertProductId($productId);
                        $item->setAlertCustomerEmail($email);
                        $item->setAlertCustomerName($customerName);
                        $item->setAlertStatus('1');
                        $item->setAlertSubscriptionDate($subscriptionDate);
                        $item->save();
                        $this->messageManager->addSuccessMessage(__('You subscribed successfully for this product.'));
                    } catch (\Exception $e) {
                        $this->messageManager->addError($e->getMessage());

                    }
                } else {
                    $this->messageManager->addWarningMessage(__('This Product is In Stock.'));
                }
            } else {
                $this->messageManager->addWarningMessage(__("This Subscription already exists."));
            }
        } //new
        else {
            if (!$this->_helper->subscriptionExists($productId, $email)) {
                if ($this->_helper->isOutOfStock($productId)) {

                    try {

                        $productName = $product->getName();
                        $model->setAlertProductName($productName);
                        $model->setAlertProductId($productId);
                        $model->setAlertCustomerEmail($email);
                        $model->setAlertCustomerName($customerName);
                        $model->setAlertStatus('1');
                        $model->setAlertSubscriptionDate($subscriptionDate);
                        $model->save();
                        $alertId = $model->getAlertId();
                        $this->messageManager->addSuccessMessage(__('You subscribed successfully for this product.'));
                    } catch (\Exception $e) {
                        $this->messageManager->addError($e->getMessage());

                    }
                } else {
                    $this->messageManager->addWarningMessage(__('This Product is In Stock.'));
                }

            } else {
                $this->messageManager->addWarningMessage(__('This Subscription already exists.'));
            }
        }


        if ($go_back) {
            $url = $this->getUrl("*/*/edit", array("alert_id" => $alertId));
            $this->_redirect($url);
        } else {
            $this->_redirect("*/grid/");
        }
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Shero_Notifications::manage_notifications');
    }
}
