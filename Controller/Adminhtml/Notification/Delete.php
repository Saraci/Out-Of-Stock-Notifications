<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: delete controller in admin
 */
namespace Shero\Notifications\Controller\Adminhtml\Notification;

use Shero\Notifications\Model\NotificationsFactory;

class Delete extends \Magento\Backend\App\Action
{

    /**
     * @var NotificationsFactory
     */
    protected $_notificationsFactory;


    /**
     * Delete constructor.
     * @param NotificationsFactory $notificationsFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct
    (
        NotificationsFactory $notificationsFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->_notificationsFactory = $notificationsFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('alert_id');

        try {
            // init model and delete
            $model = $this->_notificationsFactory->create();
            $model->load($id);
            $model->delete();

            // display success message
            $this->messageManager->addSuccess(__('You deleted the subscription.'));
            // go to grid
            $url = $this->getUrl("*/grid");

            $this->_redirect($url);

        } catch (\Exception $e) {
            // display error message
            $this->messageManager->addError($e->getMessage());
            // go back to edit form
            $url = $this->getUrl("*/*/edit", array("alert_id" => $id));
            $this->_redirect($url);
        }

    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Shero_Notifications::manage_notifications');
    }
}
