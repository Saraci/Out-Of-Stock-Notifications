<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Notifications
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 * @description: cron to send emails to admin and customers
 */
namespace Shero\Notifications\Cron;

use Shero\Notifications\Model\ResourceModel\Notifications\CollectionFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\App\Action\Context;

class Notifications
{

    protected $_collectionFactory;
    protected $_logger;


    public function __construct(
        Context $context,
        CollectionFactory $_collectionFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Shero\Notifications\Helper\Data $helperData,
        TransportBuilder $_transportBuilder,
        \Psr\Log\LoggerInterface $_logger,
        array $data = []
    )
    {
        $this->_collectionFactory = $_collectionFactory;
        $this->messageManager = $messageManager;
        $this->_logger = $_logger;
        $this->_helper = $helperData;
        $this->_transportBuilder = $_transportBuilder;

    }

    /**
     * send email to subscribed customer for back in stock products
     */
    public function notifyCustomers()
    {
        $senderEmail = $this->_helper->getAdminSenderEmail();
        $customerEmailTemplate = $this->_helper->getEmailNotificationTemplate();
        $collection = $this->_collectionFactory->create();

        foreach ($collection as $item) {

            $id = $item->getAlertProductId();
            /**
             * @var $product | Magento\Catalog\Model\Product
             */
            $product = $this->_helper->getProductById($id);
            $sku = $product->getSku();
            $productName = $product->getName();
            $productLink = $this->_helper->getProductUrl($product);
            $customerEmail = $item->getAlertCustomerEmail();


            //check if item is back in stock
            $isOutOfStock = !$this->_helper->isOutOfStock($id);


            if ($isOutOfStock && $item->getAlertStatus()) {



                $variables = [
                    'sku' => $sku,
                    'product_name' => $productName,
                    'product_link' => $productLink

                ];
                try {

                    $postObject = new \Magento\Framework\DataObject();
                    $postObject->setData($variables);
                    $transport = $this->_transportBuilder
                        ->setTemplateIdentifier($customerEmailTemplate)
                        ->setTemplateOptions(
                            [
                            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                            'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                            ])
                        ->setTemplateVars($variables)
                        ->setFrom(['name' => $this->_helper->getAdminSenderName(), 'email' => $senderEmail])
                        ->addTo($customerEmail)
                        ->getTransport();
                    $transport->sendMessage();

                    //make subscription inactive after email is sent
                    $item->setAlertStatus(0);
                    $item->save();


                    $this->_logger->info('Out of Stock notifications email sent.');

                } catch (\Exception $e) {

                    $this->_logger->error($e->getMessage());

                }

            }
        }
    }

    /**
     * send email to admins when product has low stock
     */
    public function notifyAdmins()
    {

        $senderEmail = $this->_helper->getAdminSenderEmail();
        $adminEmails = $this->_helper->getAdminReceiverEmails();
        $emailTemplateAdmin = $this->_helper->getAdminEmailTemplate();
        $variables = array();

        if (count($this->_helper->getOutOfStockProducts())) {
            try {

                $postObject = new \Magento\Framework\DataObject();
                $postObject->setData($variables);
                /**
                 * @var  $transport Magento\Framework\Mail\Template\TransportBuilder
                 */
                $transport = $this->_transportBuilder
                    ->setTemplateIdentifier($emailTemplateAdmin)
                    ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])
                    ->setTemplateVars($variables)
                    ->setFrom(['name' => $this->_helper->getAdminSenderName(), 'email' => $senderEmail])
                    ->addTo($adminEmails)
                    ->getTransport();
                $transport->sendMessage();
                $this->_logger->info('Out of Stock notification email sent.');


            } catch (\Exception $e) {

                $this->_logger->error($e->getMessage());
                return;
            }

        }
    }

}